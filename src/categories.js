

const categories = [

    {
        path: '/categorias/medicina',
        name: 'MEDICINAS',
        show:false,
        url: 'https://firebasestorage.googleapis.com/v0/b/saludsmart-9edc2.appspot.com/o/salud-medicina-tablet.jpeg?alt=media&token=a065a2fd-4028-4ec1-a870-c935ee92c84f'
    },
    {
        path: '/categorias/ejercicio',
        name: 'EJERCICIOS',
        show:false,
        url: 'https://firebasestorage.googleapis.com/v0/b/saludsmart-9edc2.appspot.com/o/Fotolia_58913397_Subscription_Monthly_M.jpeg?alt=media&token=2cc18e34-d39d-4df2-9b94-fad384fa96b9'
    },
    {
        path: '/categorias/consejo',
        name: 'CONSEJOS',
        show:false,
        url: 'https://firebasestorage.googleapis.com/v0/b/saludsmart-9edc2.appspot.com/o/medicina-preventiva.jpeg?alt=media&token=e8baae3d-b673-4ec9-bf88-836eaac27bca'
    },
    {
        path: '/categorias/habito',
        name: 'HABITOS',
        show:false,
        url: 'https://firebasestorage.googleapis.com/v0/b/saludsmart-9edc2.appspot.com/o/nino-naranjas.jpeg?alt=media&token=e3f67249-9445-457f-8d44-6b83604b6e4d'
    }
    ,
    {
        path: '/categorias/tratamiento',
        name: 'TRATAMIENTOS',
        show:false,
        url: 'https://firebasestorage.googleapis.com/v0/b/saludsmart-9edc2.appspot.com/o/bigstock-pelo-cabello-tratamiento-natural-.jpeg?alt=media&token=981216f4-6139-432f-83fe-e91a673c1f77'
    },
    {
        path: '/categorias/nutricion',
        name: 'NUTRICION',
        show:false,
        url: 'https://firebasestorage.googleapis.com/v0/b/saludsmart-9edc2.appspot.com/o/nutricio%CC%81n.jpeg?alt=media&token=bdd04b9b-7402-4187-a59a-0db96d6ea675'
    }

];


export  default  categories
